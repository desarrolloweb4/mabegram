FROM node:latest as node
WORKDIR /angular
COPY ./ /angular/
RUN npm install --no-optional
RUN npm run build -- --prod
RUN apt-get update
RUN apt-get install nano net-tools vim

FROM nginx:alpine
COPY --from=node /angular/dist/WeSite /usr/share/nginx/html
