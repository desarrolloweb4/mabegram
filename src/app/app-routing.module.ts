import { UpdateComponent } from './componentes/modals/update/update.component';
import { PrincipalComponent } from './componentes/principal/principal.component';
import { RegisterComponent } from './componentes/user/register/register.component';
import { ChatComponent } from './componentes/chat/chat.component';
import { LoginComponent } from './componentes/user/login/login.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  // { path: '', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'chat', component: ChatComponent },
  { path: 'publicacion/:id', component: UpdateComponent },
  // tslint:disable-next-line:comment-format
  //{ path: 'principal', component: PrincipalComponent },
  { path: '', component: PrincipalComponent },
  // tslint:disable-next-line:whitespace
  { path: '**', pathMatch:'full', redirectTo:'' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
