import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

import { PublicacionService } from './../../../services/apispringboot/publicacion/publicacion.service';
import { PublicacionnodeService } from './../../../services/apinodejs/publicacionnode/publicacionnode.service';
import { Publicacion } from './../../../models/publicacion';

@Component({
  selector: 'site-imagen',
  templateUrl: './imagen.component.html',
  styleUrls: ['./imagen.component.scss']
})
export class ImagenComponent implements OnInit {

  publicacion: Publicacion = {
    imagen: '',
    descripcion: ''
  // tslint:disable-next-line:semicolon
  }

  constructor(private publicacionService: PublicacionService,
              private publicacionServiceNo: PublicacionnodeService,
              private router: Router) { }

  ngOnInit(): void {
  }

  GuardarPublicacionwithSpring(): void{
    delete this.publicacion.id;
    this.publicacionService.createPublicacion(this.publicacion)
      .subscribe(
        res => {
          console.log(res);
          this.router.navigate(['/']);
        },
        err => console.error(err)
      );
  }

  GuardarPublicacionNode(): void {
    delete this.publicacion.id;
    this.publicacionServiceNo.createPublicacion(this.publicacion)
      .subscribe(
        res => {
          console.log(res);
        },
        err => console.error(err)
      );
  }

  OnFileChange(): void{

  }
}
