import { Messages } from './../../models/messages';
import { MessageService } from './../../services/apispringboot/message/message.service';
import { Component, OnInit } from '@angular/core';
import * as io from 'socket.io-client';

const socket = 'localhost:3000';
@Component({
  selector: 'site-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss']
})
export class ChatComponent implements OnInit {
  soc;
  message: string;

  messages: Messages = {
    message: ''
  // tslint:disable-next-line:semicolon
  }

  constructor(private messageService: MessageService) { }

  ngOnInit(): void {
    this.Prueba();
  }

  Prueba(): void{
    this.soc = io(socket);
    this.soc.on('messages', (data: string) => {
      if (data) {
       const element = document.createElement('li');
       element.innerHTML = data;
       element.style.background = 'white';
       element.style.padding =  '15px 30px';
       element.style.margin = '10px';
       document.getElementById('message-list').appendChild(element);
       }
     });
  }

  SendMessage(): void {
    this.soc.emit('message', this.message);
    const element = document.createElement('li');
    element.innerHTML = this.message;
    element.style.background = 'white';
    element.style.padding =  '15px 30px';
    element.style.margin = '10px';
    element.style.textAlign = 'right';
    document.getElementById('message-list').appendChild(element);
    this.messageService.createMessage(this.messages)
      .subscribe(
        res => {
          console.log(res);
        },
        err => console.error(err)
      );
    this.message = '';
 }

}
