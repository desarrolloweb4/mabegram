import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Messages } from '../../../models/messages';

@Injectable({
  providedIn: 'root'
})
export class MessageService {

  // tslint:disable-next-line:indent
	url = 'http://localhost:2020/message';

  constructor(private http: HttpClient) { }

  // tslint:disable-next-line:comment-format
  // tslint:disable-next-line:typedef
  getMessages(){
    return this.http.get(`${this.url}/all`);
  }

  // tslint:disable-next-line:comment-format
  //tslint:disable-next-line:typedef
  getMessage(id: string){
    return this.http.get(`${this.url}/${id}`);
  }

  // tslint:disable-next-line:typedef
  createMessage(message: Messages){
    return this.http.post(`${this.url}/create`, message);
  }

  // tslint:disable-next-line:typedef
  // updateMessage(updateMessage: Messages): Observable<Messages>{
  //   return this.http.put(`${this.url}/update`, updateMessage);
  // }

  // tslint:disable-next-line:typedef
  deleteMessage(id: string){
    return this.http.delete(`${this.url}/${id}`);
  }


}
