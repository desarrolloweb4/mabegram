import { TestBed } from '@angular/core/testing';

import { PublicacionnodeService } from './publicacionnode.service';

describe('PublicacionnodeService', () => {
  let service: PublicacionnodeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PublicacionnodeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
