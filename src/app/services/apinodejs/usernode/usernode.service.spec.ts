import { TestBed } from '@angular/core/testing';

import { UsernodeService } from './usernode.service';

describe('UsernodeService', () => {
  let service: UsernodeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(UsernodeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
