import { Observable } from 'rxjs';
import { User } from './../../../models/user';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UsernodeService {

  url = 'http://localhost:3000/api/user';

  constructor(private http: HttpClient) { }

  // tslint:disable-next-line:comment-format
  // tslint:disable-next-line:typedef
  getPublicaciones(){
    return this.http.get(`${this.url}`);
  }

  // tslint:disable-next-line:comment-format
  //tslint:disable-next-line:typedef
  getPublicacion(id: string){
    return this.http.get(`${this.url}/${id}`);
  }

  // tslint:disable-next-line:typedef
  createPublicacion(user: User){
    return this.http.post(`${this.url}`, user);
  }

  // tslint:disable-next-line:typedef
  updatePublicacion(id: string|number, actualiaruser: User): Observable<User>{
    return this.http.put(`${this.url}/${id}`, actualiaruser);
  }

  // tslint:disable-next-line:typedef
  deletePublicacion(id: string){
    return this.http.delete(`${this.url}/${id}`);
  }

}
