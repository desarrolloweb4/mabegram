import { Observable } from 'rxjs';
import { Messages } from './../../../models/messages';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MessagenodeService {

  url = 'http://localhost:3000/api/message';

  constructor(private http: HttpClient) { }

  // tslint:disable-next-line:comment-format
  // tslint:disable-next-line:typedef
  getPublicaciones(){
    return this.http.get(`${this.url}`);
  }

  // tslint:disable-next-line:comment-format
  //tslint:disable-next-line:typedef
  getPublicacion(id: string){
    return this.http.get(`${this.url}/${id}`);
  }

  // tslint:disable-next-line:typedef
  createPublicacion(message: Messages){
    return this.http.post(`${this.url}`, message);
  }

  // tslint:disable-next-line:typedef
  updatePublicacion(id: string|number, actualizarmenssage: Messages): Observable<Messages>{
    return this.http.put(`${this.url}/${id}`, actualizarmenssage);
  }

  // tslint:disable-next-line:typedef
  deletePublicacion(id: string){
    return this.http.delete(`${this.url}/${id}`);
  }

}
