import { TestBed } from '@angular/core/testing';

import { MessagenodeService } from './messagenode.service';

describe('MessagenodeService', () => {
  let service: MessagenodeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MessagenodeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
