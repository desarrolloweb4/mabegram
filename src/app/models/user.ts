export interface User {
	id?: number;
	nombre?: string;
	nickname?: string;
	imagen?: string;
}
