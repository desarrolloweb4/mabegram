import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './componentes/user/login/login.component';
import { RegisterComponent } from './componentes/user/register/register.component';
import { ChatComponent } from './componentes/chat/chat.component';
import { PrincipalComponent } from './componentes/principal/principal.component';
import { ImagenComponent } from './componentes/modals/imagen/imagen.component';
import { PublicacionComponent } from './componentes/publicacion/publicacion.component';
import { UpdateComponent } from './componentes/modals/update/update.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    ChatComponent,
    PrincipalComponent,
    ImagenComponent,
    PublicacionComponent,
    UpdateComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
